<?php declare(strict_types=1);

namespace Plugin\jtl_rapi;

use JTL\Cache\JTLCacheInterface;
use JTL\Consent\ConsentModel;
use JTL\DB\DbInterface;
use JTL\REST\Controllers\AbstractController;
use Laminas\Diactoros\Response\TextResponse;
use League\Fractal\Manager;
use League\Route\RouteGroup;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class TestController extends AbstractController
{
    public function __construct(Manager $fractal, DbInterface $db, JTLCacheInterface $cache)
    {
        $this->modelClass = ConsentModel::class;
        parent::__construct($this->modelClass, $fractal, $db, $cache);
    }

    public function registerRoutes(RouteGroup $routeGroup): void
    {
        $routeGroup->get('/test', $this->index(...));
        $routeGroup->get('/test/{id}', $this->show(...));
        $routeGroup->put('/test/{id}', $this->update(...));
        $routeGroup->post('/test', $this->create(...));
        $routeGroup->delete('/test/{id}', $this->delete(...));
    }

    public function index(ServerRequestInterface $request, array $params): ResponseInterface
    {
        return (new TextResponse('index!'));
    }

    public function show2(ServerRequestInterface $request, array $params): ResponseInterface
    {
        return (new TextResponse('show!'));
    }

    public function update(ServerRequestInterface $request, array $params): ResponseInterface
    {
        return new TextResponse('update!');
    }

    public function create(ServerRequestInterface $request, array $params): ResponseInterface
    {
        return new TextResponse('Create!');
    }

    public function delete(ServerRequestInterface $request, array $params): ResponseInterface
    {
        return new TextResponse('Delete!');
    }
}
