<?php declare(strict_types=1);

namespace Plugin\jtl_rapi;

use JTL\Events\Dispatcher;
use JTL\Plugin\Bootstrapper;

/**
 * Class Bootstrap
 * @package Plugin\jtl_rapi
 */
class Bootstrap extends Bootstrapper
{
    public function boot(Dispatcher $dispatcher)
    {
        parent::boot($dispatcher);
        if (!\defined('\HOOK_RESTAPI_REGISTER_CONTROLLER')) {
            return;
        }
        $dispatcher->hookInto(\HOOK_RESTAPI_REGISTER_CONTROLLER, static function (array $classess): array {
            $classess[] = TestController::class;
            return $classess;
        });
    }
}
